import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import resetPassword from '@/views/resetPassword/resetPassword.vue'
import registerSucess from '@/views/registerSucess/registerSucess.vue'
import expired from '@/views/expired/expired.vue'
import resetSucess from '@/views/resetSucess/resetSucess.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'resetPassword',
    component: resetPassword
  },
  // {
  //   path: '/1',
  //   name: 'registerSucess',
  //   component: registerSucess
  // },
  // {
  //   path: '/2',
  //   name: 'expired',
  //   component: expired
  // },
  // {
  //   path: '/3',
  //   name: 'resetSucess',
  //   component: resetSucess
  // },

  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
