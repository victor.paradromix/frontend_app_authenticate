import { defineComponent, ref } from "vue";
import { useField, useForm } from 'vee-validate';
import HelloWorld from "@/components/HelloWorld.vue"; // @ is an alias to /src
import * as yup from 'yup';
export default defineComponent({
  name: "Home",
  components: {},
  setup() {

    let passwordShow = ref(false);
    let checkPasswordShow = ref(false);

    const schema = yup.object({
      password: yup.string().matches(/^.*(?=.{8,})((?=.*[a-z]){1})(?=.*\d)/, '密碼格式不符合規定，須為8碼以上英文與數字混合字串').
        required('確認新密碼不能為空，請輸入密碼'),
      checkPassword: yup.string()
        .oneOf([yup.ref('password'), null], '確認新密碼不一致，請重新輸入').required('確認新密碼不能為空，請輸入密碼')
    });
    const { handleSubmit } = useForm({
      validationSchema: schema,
    });
    const onSubmit = handleSubmit((values, { resetForm }) => {

      alert(JSON.stringify(values, null, 2));
      resetForm();
    });
    // No need to define rules for fields
    const { value: checkPassword, errorMessage: checkPasswordError } = useField('checkPassword');
    const { value: password, errorMessage: passwordError } = useField('password');




    return {
      password, checkPassword, passwordShow, onSubmit,
      checkPasswordShow, passwordError, checkPasswordError
    };
  }

});
